local util = require("starship.util")

-- Load the theme
local set = function()
	util.load()
end

return { set = set }
