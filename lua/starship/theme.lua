local starship = require("starship.colors")

local theme = {}

theme.loadSyntax = function()
  -- Syntax highlight groups
  local syntax = {
    Type = { fg = starship.theme9 },                                                   -- int, long, char, etc.
    StorageClass = { fg = starship.theme9 },                                           -- static, register, volatile, etc.
    Structure = { fg = starship.theme9 },                                              -- struct, union, enum, etc.
    Constant = { fg = starship.theme_light },                                          -- any constant
    Character = { fg = starship.theme_green },                                         -- any character constant: 'c', '\n'
    Number = { fg = starship.theme_purple },                                           -- a number constant: 5
    Boolean = { fg = starship.theme_blue },                                            -- a boolean constant: TRUE, false
    Float = { fg = starship.theme_purple },                                            -- a floating point constant: 2.3e10
    Statement = { fg = starship.theme_blue },                                          -- any statement
    Label = { fg = starship.theme_blue },                                              -- case, default, etc.
    Operator = { fg = starship.theme_blue },                                           -- sizeof", "+", "*", etc.
    Exception = { fg = starship.theme_blue },                                          -- try, catch, throw
    PreProc = { fg = starship.theme_blue },                                            -- generic Preprocessor
    Include = { fg = starship.theme_blue },                                            -- preprocessor #include
    Define = { fg = starship.theme9 },                                                 -- preprocessor #define
    Macro = { fg = starship.theme9 },                                                  -- same as Define
    Typedef = { fg = starship.theme9 },                                                -- A typedef
    PreCondit = { fg = starship.theme_yellow },                                        -- preprocessor #if, #else, #endif, etc.
    Special = { fg = starship.theme_light },                                           -- any special symbol
    SpecialChar = { fg = starship.theme_yellow },                                      -- special character in a constant
    Tag = { fg = starship.theme_light },                                               -- you can use CTRL-] on this
    Delimiter = { fg = starship.theme_light },                                         -- character that needs attention like , or .
    SpecialComment = { fg = starship.theme_blue },                                     -- special things inside a comment
    Debug = { fg = starship.theme_red },                                               -- debugging statements
    Underlined = { fg = starship.theme_green, bg = starship.none, style = "underline" }, -- text that stands out, HTML links
    Ignore = { fg = starship.theme2 },                                                 -- left blank, hidden
    Error = { fg = starship.theme_red, bg = starship.none, style = "bold,underline" }, -- any erroneous construct
    Todo = { fg = starship.theme_yellow, bg = starship.none, style = "bold,italic" },  -- anything that needs extra attention; mostly the keywords TODO FIXME and XXX
    Conceal = { fg = starship.none, bg = starship.theme5 },

    htmlLink = { fg = starship.theme_green, style = "underline" },
    htmlH1 = { fg = starship.theme_blue, style = "bold" },
    htmlH2 = { fg = starship.theme_red, style = "bold" },
    htmlH3 = { fg = starship.theme_green, style = "bold" },
    htmlH4 = { fg = starship.theme_purple, style = "bold" },
    htmlH5 = { fg = starship.theme_blue, style = "bold" },
    markdownH1 = { fg = starship.theme_blue, style = "bold" },
    markdownH2 = { fg = starship.theme_red, style = "bold" },
    markdownH3 = { fg = starship.theme_green, style = "bold" },
    markdownH1Delimiter = { fg = starship.theme_blue },
    markdownH2Delimiter = { fg = starship.theme_red },
    markdownH3Delimiter = { fg = starship.theme_green },
  }

  -- Italic comments
  if vim.g.starship_italic == false then
    syntax.Comment = { fg = starship.theme8 }                                             -- normal comments
    syntax.Conditional = { fg = starship.theme_blue }                                     -- normal if, then, else, endif, switch, etc.
    syntax.Function = { fg = starship.theme_blue }                                        -- normal function names
    syntax.Identifier = { fg = starship.theme_blue }                                      -- any variable name
    syntax.Keyword = { fg = starship.theme9 }                                             -- normal for, do, while, etc.
    syntax.Repeat = { fg = starship.theme_blue }                                          -- normal any other keyword
    syntax.String = { fg = starship.theme_green }                                         -- any string
  else
    syntax.Comment = { fg = starship.theme8, bg = starship.none, style = "italic" }       -- italic comments
    syntax.Conditional = { fg = starship.theme_blue, bg = starship.none, style = "italic" } -- italic if, then, else, endif, switch, etc.
    syntax.Function = { fg = starship.theme_blue, bg = starship.none, style = "italic" }  -- italic funtion names
    syntax.Identifier = { fg = starship.theme_blue, bg = starship.none, style = "italic" } -- any variable name
    syntax.Keyword = { fg = starship.theme_blue, bg = starship.none, style = "italic" }   -- italic for, do, while, etc.
    syntax.Repeat = { fg = starship.theme_blue, bg = starship.none, style = "italic" }    -- italic any other keyword
    syntax.String = { fg = starship.theme_green, bg = starship.none, style = "italic" }   -- any string
  end

  return syntax
end

theme.loadEditor = function()
  -- Editor highlight groups

  local editor = {
    MyHighlight = { fg = starship.theme_purple, bg = starship.theme_purple },         -- normal text and background color
    NormalFloat = { fg = starship.theme_light, bg = starship.float },                 -- normal text and background color
    FloatBorder = { fg = starship.float, bg = starship.float },                       -- normal text and background color
    ColorColumn = { fg = starship.none, bg = starship.theme2 },                       --  used for the columns set with 'colorcolumn'
    Conceal = { fg = starship.theme2 },                                               -- placeholder characters substituted for concealed text (see 'conceallevel')
    Cursor = { fg = starship.theme_light, bg = starship.none, style = "reverse" },    -- the character under the cursor
    CursorIM = { fg = starship.theme_light, bg = starship.none, style = "reverse" },  -- like Cursor, but used when in IME mode
    Directory = { fg = starship.theme_blue, bg = starship.none },                     -- directory names (and other special names in listings)
    DiffAdd = { fg = starship.theme_green, bg = starship.none, style = "reverse" },   -- diff mode: Added line
    DiffChange = { fg = starship.theme_yellow, bg = starship.none, style = "reverse" }, --  diff mode: Changed line
    DiffDelete = { fg = starship.theme_red, bg = starship.none, style = "reverse" },  -- diff mode: Deleted line
    DiffText = { fg = starship.theme_purple, bg = starship.none, style = "reverse" }, -- diff mode: Changed text within a changed line
    EndOfBuffer = { fg = starship.theme5 },
    ErrorMsg = { fg = starship.none },
    Folded = { fg = starship.theme8, bg = starship.none, style = "italic" },
    FoldColumn = { fg = starship.theme_dark },
    IncSearch = { fg = starship.theme2, bg = starship.theme_green },
    LineNr = { fg = starship.theme7 },
    CursorLineNr = { fg = starship.theme_dark, style = "bold" },
    MatchParen = { fg = starship.theme_purple, bg = starship.none, style = "bold" },
    ModeMsg = { fg = starship.theme_light },
    MoreMsg = { fg = starship.theme_light },
    NonText = { fg = starship.theme_brown },
    Pmenu = { fg = starship.theme_light, bg = starship.theme7 },
    PmenuSel = { fg = starship.theme_light, bg = starship.theme8 },
    PmenuSbar = { fg = starship.theme7, bg = starship.theme_dark },
    PmenuThumb = { fg = starship.theme_light, bg = starship.theme_light },
    Question = { fg = starship.theme_green },
    QuickFixLine = { fg = starship.theme_light, bg = starship.none, style = "reverse" },
    qfLineNr = { fg = starship.theme_light, bg = starship.none, style = "reverse" },
    Search = { fg = starship.theme2, bg = starship.theme_green },
    SpecialKey = { fg = starship.theme_blue },
    SpellBad = { fg = starship.theme_red, bg = starship.none, style = "italic,undercurl" },
    SpellCap = { fg = starship.theme_blue, bg = starship.none, style = "italic,undercurl" },
    SpellLocal = { fg = starship.theme_blue, bg = starship.none, style = "italic,undercurl" },
    SpellRare = { fg = starship.theme_blue, bg = starship.none, style = "italic,undercurl" },
    WinSeparator = { fg = starship.theme3, bg = starship.none },

    StatusLine = { fg = starship.theme_dark, bg = starship.theme3 },
    StatusLineSeparator = { fg = starship.theme7, bg = starship.theme3 },
    StatusLineNC = { fg = starship.theme2, bg = starship.theme1 },
    StatusLineTerm = { fg = starship.theme_light, bg = starship.theme_dark },
    StatusLineTermNC = { fg = starship.theme_light, bg = starship.theme3 },
    StatusLineFileName = { fg = starship.theme_light, bg = starship.theme3 },
    StatusLineGitBranch = { fg = starship.theme_dark, bg = starship.theme3 },

    TabLineFill = { fg = starship.theme_dark, bg = starship.theme3 },
    TablineSel = { fg = starship.theme2, bg = starship.theme_blue },
    Tabline = { fg = starship.theme_light, bg = starship.theme2 },
    Title = { fg = starship.theme_green, bg = starship.none, style = "bold" },
    Visual = { fg = starship.none, bg = starship.theme8 },
    VisualNOS = { fg = starship.none, bg = starship.theme8 },
    WarningMsg = { fg = starship.theme_yellow },
    WildMenu = { fg = starship.theme_orange, bg = starship.none, style = "bold" },
    CursorColumn = { fg = starship.none, bg = starship.cursorlinefg },
    CursorLine = { fg = starship.none, bg = starship.cursorlinefg },
    ToolbarLine = { fg = starship.theme_light, bg = starship.theme2 },
    ToolbarButton = { fg = starship.theme_light, bg = starship.none, style = "bold" },
    NormalMode = { fg = starship.theme_blue, bg = starship.theme1 },
    InsertMode = { fg = starship.theme_yellow, bg = starship.theme1 },
    ReplacelMode = { fg = starship.theme_purple, bg = starship.theme1 },
    VisualMode = { fg = starship.theme_orange, bg = starship.theme1 },
    CommandMode = { fg = starship.theme_green, bg = starship.theme1 },
    Warnings = { fg = starship.theme_yellow },
    VertSplit = { fg = starship.theme3 },

    healthError = { fg = starship.theme_red },
    healthSuccess = { fg = starship.theme_green },
    healthWarning = { fg = starship.theme_yellow },

    -- dashboard
    DashboardShortCut = { fg = starship.theme_blue },
    DashboardHeader = { fg = starship.theme_blue },
    DashboardCenter = { fg = starship.theme_blue },
    DashboardFooter = { fg = starship.theme_green, style = "italic" },
  }

  -- Options:

  --Set transparent background
  if vim.g.starship_disable_background then
    editor.Normal = { fg = starship.theme_light, bg = starship.none } -- normal text and background color
    editor.SignColumn = { fg = starship.theme_light, bg = starship.none }
    editor.MsgArea = { fg = starship.theme_light, bg = starship.none }
  else
    editor.Normal = { fg = starship.theme_light, bg = starship.theme5 } -- normal text and background color
    editor.SignColumn = { fg = starship.theme_light, bg = starship.theme5 }
    editor.MsgArea = { fg = starship.theme_light, bg = starship.theme5 }
  end

  return editor
end

theme.loadTerminal = function()
  vim.g.terminal_color_0 = starship.theme2
  vim.g.terminal_color_1 = starship.theme_red
  vim.g.terminal_color_2 = starship.theme_green
  vim.g.terminal_color_3 = starship.theme_yellow
  vim.g.terminal_color_4 = starship.theme_blue
  vim.g.terminal_color_5 = starship.theme_purple
  vim.g.terminal_color_6 = starship.theme_blue
  vim.g.terminal_color_7 = starship.theme_light
  vim.g.terminal_color_8 = starship.theme1
  vim.g.terminal_color_9 = starship.theme_red
  vim.g.terminal_color_10 = starship.theme_green
  vim.g.terminal_color_11 = starship.theme_yellow
  vim.g.terminal_color_12 = starship.theme_blue
  vim.g.terminal_color_13 = starship.theme_purple
  vim.g.terminal_color_14 = starship.theme_blue
  vim.g.terminal_color_15 = starship.theme_light
end

theme.loadTreeSitter = function()
  -- TreeSitter highlight groups
  local treesitter = {
    TSAnnotation = { fg = starship.syntax.ss7 },                                       -- For C++/Dart attributes, annotations thatcan be attached to the code to denote some kind of meta information.
    TSConstructor = { fg = starship.syntax.ss4 },                                      -- For constructor calls and definitions: `= { }` in Lua, and Java constructors.
    TSConstant = { fg = starship.syntax.ss8 },                                         -- For constants
    TSFloat = { fg = starship.syntax.ss10 },                                           -- For floats
    TSNumber = { fg = starship.syntax.ss10 },                                          -- For all number

    TSAttribute = { fg = starship.syntax.ss10 },                                       -- (unstable) TODO: docs
    TSVariable = { fg = starship.syntax.ss1, style = "bold" },                         -- Any variable name that does not have another highlight.
    TSVariableBuiltin = { fg = starship.syntax.ss1, style = "bold" },
    TSBoolean = { fg = starship.syntax.ss4, style = "bold" },                          -- For booleans.
    TSConstBuiltin = { fg = starship.syntax.ss2, style = "bold" },                     -- For constant that are built in the language: `nil` in Lua.
    TSConstMacro = { fg = starship.syntax.ss2, style = "bold" },                       -- For constants that are defined by macros: `NULL` in C.
    TSError = { fg = starship.syntax.ss6 },                                            -- For syntax/parser errors.
    TSException = { fg = starship.syntax.ss10 },                                       -- For exception related keywords.
    TSFuncMacro = { fg = starship.syntax.ss2 },                                        -- For macro defined fuctions (calls and definitions): each `macro_rules` in Rust.
    TSInclude = { fg = starship.syntax.ss4 },                                          -- For includes: `#include` in C, `use` or `extern crate` in Rust, or `require` in Lua.
    TSLabel = { fg = starship.syntax.ss10 },                                           -- For labels: `label:` in C and `:label:` in Lua.
    TSOperator = { fg = starship.syntax.ss4 },                                         -- For any operator: `+`, but also `->` and `*` in C.
    TSParameter = { fg = starship.syntax.ss5 },                                        -- For parameters of a function.
    TSParameterReference = { fg = starship.syntax.ss5 },                               -- For references to parameters of a function.
    TSPunctDelimiter = { fg = starship.syntax.ss3 },                                   -- For delimiters ie: `.`
    TSPunctBracket = { fg = starship.syntax.ss3 },                                     -- For brackets and parens.
    TSPunctSpecial = { fg = starship.syntax.ss3 },                                     -- For special punctutation that does not fall in the catagories before.
    TSSymbol = { fg = starship.syntax.ss10 },                                          -- For identifiers referring to symbols or atoms.
    TSType = { fg = starship.syntax.ss4 },                                             -- For types.
    TSTypeBuiltin = { fg = starship.syntax.ss4 },                                      -- For builtin types.
    TSTag = { fg = starship.syntax.ss1 },                                              -- Tags like html tag names.
    TSTagDelimiter = { fg = starship.syntax.ss10 },                                    -- Tag delimiter like `<` `>` `/`
    TSText = { fg = starship.syntax.ss1 },                                             -- For strings considestarship11_gui text in a markup language.
    TSTextReference = { fg = starship.syntax.ss10 },                                   -- FIXME
    TSEmphasis = { fg = starship.syntax.ss5 },                                         -- For text to be represented with emphasis.
    TSUnderline = { fg = starship.syntax.ss1, bg = starship.none, style = "underline" }, -- For text to be represented with an underline.
    TSTitle = { fg = starship.syntax.ss5, bg = starship.none, style = "bold" },        -- Text that is part of a title.
    TSLiteral = { fg = starship.syntax.ss1 },                                          -- Literal text.
    TSURI = { fg = starship.syntax.ss9 },                                              -- Any URI like a link or email.
  }

  if vim.g.starship_italic == false then
    -- Comments
    treesitter.TSComment = { fg = starship.syntax.ss11 }
    -- Conditionals
    treesitter.TSConditional = { fg = starship.syntax.ss4 } -- For keywords related to conditionnals.
    -- Function names
    treesitter.TSFunction = { fg = starship.syntax.ss3 }  -- For fuction (calls and definitions).
    treesitter.TSMethod = { fg = starship.syntax.ss2 }    -- For method calls and definitions.
    treesitter.TSFuncBuiltin = { fg = starship.syntax.ss3 }
    -- Namespaces and property accessors
    treesitter.TSNamespace = { fg = starship.syntax.ss1 } -- For identifiers referring to modules and namespaces.
    treesitter.TSField = { fg = starship.syntax.ss1 }   -- For fields in literals
    treesitter.TSProperty = { fg = starship.syntax.ss5 } -- Same as `TSField`
    -- Language keywords
    treesitter.TSKeyword = { fg = starship.syntax.ss4 } -- For keywords that don't fall in other categories.
    treesitter.TSKeywordFunction = { fg = starship.syntax.ss3 }
    treesitter.TSKeywordReturn = { fg = starship.syntax.ss3 }
    treesitter.TSKeywordOperator = { fg = starship.syntax.ss3 }
    treesitter.TSRepeat = { fg = starship.syntax.ss4 }      -- For keywords related to loops.
    -- Strings
    treesitter.TSString = { fg = starship.syntax.ss9 }      -- For strings.
    treesitter.TSStringRegex = { fg = starship.syntax.ss2 } -- For regexes.
    treesitter.TSStringEscape = { fg = starship.syntax.ss10 } -- For escape characters within a string.
    treesitter.TSCharacter = { fg = starship.syntax.ss9 }   -- For characters.
  else
    -- Comments
    treesitter.TSComment = { fg = starship.syntax.ss11, style = "italic" }
    -- Conditionals
    treesitter.TSConditional = { fg = starship.syntax.ss4, style = "italic" } -- For keywords related to conditionnals.
    -- Function names
    treesitter.TSFunction = { fg = starship.syntax.ss3, style = "italic" }  -- For fuction (calls and definitions).
    treesitter.TSMethod = { fg = starship.syntax.ss2, style = "italic" }    -- For method calls and definitions.
    treesitter.TSFuncBuiltin = { fg = starship.syntax.ss3, style = "italic" }
    -- Namespaces and property accessors
    treesitter.TSNamespace = { fg = starship.syntax.ss1, style = "italic" } -- For identifiers referring to modules and namespaces.
    treesitter.TSField = { fg = starship.syntax.ss1, style = "italic" }   -- For fields.
    treesitter.TSProperty = { fg = starship.syntax.ss5, style = "italic" } -- Same as `TSField`, but when accessing, not declaring.
    -- Language keywords
    treesitter.TSKeyword = { fg = starship.syntax.ss4, style = "italic" } -- For keywords that don't fall in other categories.
    treesitter.TSKeywordFunction = { fg = starship.syntax.ss3, style = "italic" }
    treesitter.TSKeywordReturn = { fg = starship.syntax.ss3, style = "italic" }
    treesitter.TSKeywordOperator = { fg = starship.syntax.ss3, style = "italic" }
    treesitter.TSRepeat = { fg = starship.syntax.ss4, style = "italic" }      -- For keywords related to loops.
    -- Strings
    treesitter.TSString = { fg = starship.syntax.ss9, style = "italic" }      -- For strings.
    treesitter.TSStringRegex = { fg = starship.syntax.ss2, style = "italic" } -- For regexes.
    treesitter.TSStringEscape = { fg = starship.syntax.ss10, style = "italic" } -- For escape characters within a string.
    treesitter.TSCharacter = { fg = starship.syntax.ss9, style = "italic" }   -- For characters.
  end

  return treesitter
end

theme.loadLSP = function()
  -- Lsp highlight groups

  local lsp = {
    LspDiagnosticsDefaultError = { fg = starship.theme_red },                             -- used for "Error" diagnostic virtual text
    LspDiagnosticsSignError = { fg = starship.theme_red },                                -- used for "Error" diagnostic signs in sign column
    LspDiagnosticsFloatingError = { fg = starship.theme_red },                            -- used for "Error" diagnostic messages in the diagnostics float
    LspDiagnosticsVirtualTextError = { fg = starship.theme_red },                         -- Virtual text "Error"
    LspDiagnosticsUnderlineError = { style = "undercurl", sp = starship.theme_red },      -- used to underline "Error" diagnostics.
    LspDiagnosticsDefaultWarning = { fg = starship.theme_yellow },                        -- used for "Warning" diagnostic signs in sign column
    LspDiagnosticsSignWarning = { fg = starship.theme_yellow },                           -- used for "Warning" diagnostic signs in sign column
    LspDiagnosticsFloatingWarning = { fg = starship.theme_yellow },                       -- used for "Warning" diagnostic messages in the diagnostics float
    LspDiagnosticsVirtualTextWarning = { fg = starship.theme_yellow },                    -- Virtual text "Warning"
    LspDiagnosticsUnderlineWarning = { style = "undercurl", sp = starship.theme_yellow }, -- used to underline "Warning" diagnostics.
    LspDiagnosticsDefaultInformation = { fg = starship.theme_blue },                      -- used for "Information" diagnostic virtual text
    LspDiagnosticsSignInformation = { fg = starship.theme_blue },                         -- used for "Information" diagnostic signs in sign column
    LspDiagnosticsFloatingInformation = { fg = starship.theme_blue },                     -- used for "Information" diagnostic messages in the diagnostics float
    LspDiagnosticsVirtualTextInformation = { fg = starship.theme_blue },                  -- Virtual text "Information"
    LspDiagnosticsUnderlineInformation = { style = "undercurl", sp = starship.theme_blue }, -- used to underline "Information" diagnostics.
    LspDiagnosticsDefaultHint = { fg = starship.theme_blue },                             -- used for "Hint" diagnostic virtual text
    LspDiagnosticsSignHint = { fg = starship.theme_blue },                                -- used for "Hint" diagnostic signs in sign column
    LspDiagnosticsFloatingHint = { fg = starship.theme_blue },                            -- used for "Hint" diagnostic messages in the diagnostics float
    LspDiagnosticsVirtualTextHint = { fg = starship.theme_blue },                         -- Virtual text "Hint"
    LspDiagnosticsUnderlineHint = { style = "undercurl", sp = starship.theme_blue },      -- used to underline "Hint" diagnostics.
    LspReferenceText = { fg = starship.theme_light, bg = starship.theme2 },               -- used for highlighting "text" references
    LspReferenceRead = { fg = starship.theme_light, bg = starship.theme2 },               -- used for highlighting "read" references
    LspReferenceWrite = { fg = starship.theme_light, bg = starship.theme2 },              -- used for highlighting "write" references

    DiagnosticError = { link = "LspDiagnosticsDefaultError" },
    DiagnosticWarn = { link = "LspDiagnosticsDefaultWarning" },
    DiagnosticInfo = { link = "LspDiagnosticsDefaultInformation" },
    DiagnosticHint = { link = "LspDiagnosticsDefaultHint" },
    DiagnosticVirtualTextWarn = { link = "LspDiagnosticsVirtualTextWarning" },
    DiagnosticUnderlineWarn = { link = "LspDiagnosticsUnderlineWarning" },
    DiagnosticFloatingWarn = { link = "LspDiagnosticsFloatingWarning" },
    DiagnosticSignWarn = { link = "LspDiagnosticsSignWarning" },
    DiagnosticVirtualTextError = { link = "LspDiagnosticsVirtualTextError" },
    DiagnosticUnderlineError = { link = "LspDiagnosticsUnderlineError" },
    DiagnosticFloatingError = { link = "LspDiagnosticsFloatingError" },
    DiagnosticSignError = { link = "LspDiagnosticsSignError" },
    DiagnosticVirtualTextInfo = { link = "LspDiagnosticsVirtualTextInformation" },
    DiagnosticUnderlineInfo = { link = "LspDiagnosticsUnderlineInformation" },
    DiagnosticFloatingInfo = { link = "LspDiagnosticsFloatingInformation" },
    DiagnosticSignInfo = { link = "LspDiagnosticsSignInformation" },
    DiagnosticVirtualTextHint = { link = "LspDiagnosticsVirtualTextHint" },
    DiagnosticUnderlineHint = { link = "LspDiagnosticsUnderlineHint" },
    DiagnosticFloatingHint = { link = "LspDiagnosticsFloatingHint" },
    DiagnosticSignHint = { link = "LspDiagnosticsSignHint" },
  }

  return lsp
end

theme.loadPlugins = function()
  -- Plugins highlight groups

  local plugins = {

    -- Lualine
    lualine_a_normal = { fg = starship.theme_purple, bg = starship.theme_yellow },

    -- LspTrouble
    LspTroubleText = { fg = starship.theme_light },
    LspTroubleCount = { fg = starship.theme_blue, bg = starship.none },
    TroubleCount = { fg = starship.theme_blue, bg = starship.none, style = "bold" },
    LspTroubleNormal = { fg = starship.theme_light, bg = starship.sidebar },

    -- Diff
    diffAdded = { fg = starship.theme_green },
    diffRemoved = { fg = starship.theme_red },
    diffChanged = { fg = starship.theme_purple },
    diffOldFile = { fg = starship.yelow },
    diffNewFile = { fg = starship.theme_orange },
    diffFile = { fg = starship.theme_blue },
    diffLine = { fg = starship.theme1 },
    diffIndexLine = { fg = starship.theme_blue },

    -- Neogit
    NeogitBranch = { fg = starship.theme_blue },
    NeogitRemote = { fg = starship.theme_blue },
    NeogitHunkHeader = { fg = starship.theme_blue },
    NeogitHunkHeaderHighlight = { fg = starship.theme_blue, bg = starship.theme2 },
    NeogitDiffContextHighlight = { bg = starship.theme2 },
    NeogitDiffDeleteHighlight = { fg = starship.theme_red, style = "reverse" },
    NeogitDiffAddHighlight = { fg = starship.theme_green, style = "reverse" },

    -- GitGutter
    GitGutterAdd = { fg = starship.theme_green },   -- diff mode: Added line |diff.txt|
    GitGutterChange = { fg = starship.theme_purple }, -- diff mode: Changed line |diff.txt|
    GitGutterDelete = { fg = starship.theme_red },  -- diff mode: Deleted line |diff.txt|

    -- GitSigns
    GitSignsAdd = { fg = starship.theme_green },       -- diff mode: Added line |diff.txt|
    GitSignsAddNr = { fg = starship.theme_green },     -- diff mode: Added line |diff.txt|
    GitSignsAddLn = { fg = starship.theme_green },     -- diff mode: Added line |diff.txt|
    GitSignsChange = { fg = starship.theme_purple },   -- diff mode: Changed line |diff.txt|
    GitSignsChangeNr = { fg = starship.theme_purple }, -- diff mode: Changed line |diff.txt|
    GitSignsChangeLn = { fg = starship.theme_purple }, -- diff mode: Changed line |diff.txt|
    GitSignsDelete = { fg = starship.theme_red },      -- diff mode: Deleted line |diff.txt|
    GitSignsDeleteNr = { fg = starship.theme_red },    -- diff mode: Deleted line |diff.txt|
    GitSignsDeleteLn = { fg = starship.theme_red },    -- diff mode: Deleted line |diff.txt|
    GitSignsCurrentLineBlame = { fg = starship.theme8 }, -- diff mode: Deleted line |diff.txt|

    -- Telescope
    TelescopePromptBorder = { fg = starship.theme_blue },
    TelescopeResultsBorder = { fg = starship.theme_blue },
    TelescopePreviewBorder = { fg = starship.theme_green },
    TelescopeSelectionCaret = { fg = starship.theme_blue },
    TelescopeSelection = { fg = starship.theme_blue },
    TelescopeMatching = { fg = starship.theme_blue },

    -- NvimTree
    NvimTreeRootFolder = { fg = starship.theme_blue, style = "bold" },
    NvimTreeGitDirty = { fg = starship.theme_purple },
    NvimTreeGitNew = { fg = starship.theme_green },
    NvimTreeImageFile = { fg = starship.theme_purple },
    NvimTreeExecFile = { fg = starship.theme_green },
    NvimTreeSpecialFile = { fg = starship.theme_blue, style = "underline" },
    NvimTreeFolderName = { fg = starship.theme_light },
    NvimTreeEmptyFolderName = { fg = starship.theme_light },
    NvimTreeFolderIcon = { fg = starship.theme_blue },
    NvimTreeIndentMarker = { fg = starship.theme_blue },

    LspDiagnosticsError = { fg = starship.theme_red, bg = starship.theme3 },
    LspDiagnosticsWarning = { fg = starship.theme_yellow, bg = starship.theme3 },
    LspDiagnosticsInformation = { fg = starship.theme_blue, bg = starship.theme3 },
    LspDiagnosticsHint = { fg = starship.theme_blue, bg = starship.theme3 },

    -- WhichKey
    WhichKey = { fg = starship.theme_light, style = "bold" },
    WhichKeyGroup = { fg = starship.theme_light },
    WhichKeyDesc = { fg = starship.theme_blue, style = "italic" },
    WhichKeySeperator = { fg = starship.theme_light },
    WhichKeyFloating = { bg = starship.float },
    WhichKeyFloat = { bg = starship.float },

    -- LspSaga
    DiagnosticError = { fg = starship.theme_red },
    DiagnosticWarning = { fg = starship.theme_yellow },
    DiagnosticInformation = { fg = starship.theme_blue },
    DiagnosticHint = { fg = starship.theme_blue },
    DiagnosticTruncateLine = { fg = starship.theme_light },
    LspFloatWinNormal = { bg = starship.theme_dark },
    LspFloatWinBorder = { fg = starship.theme_blue },
    LspSagaBorderTitle = { fg = starship.theme_blue },
    LspSagaHoverBorder = { fg = starship.theme_blue },
    LspSagaRenameBorder = { fg = starship.theme_green },
    LspSagaDefPreviewBorder = { fg = starship.theme_green },
    LspSagaCodeActionBorder = { fg = starship.theme_blue },
    LspSagaFinderSelection = { fg = starship.theme_green },
    LspSagaCodeActionTitle = { fg = starship.theme_blue },
    LspSagaCodeActionContent = { fg = starship.theme_blue },
    LspSagaSignatureHelpBorder = { fg = starship.theme_yellow },
    ReferencesCount = { fg = starship.theme_blue },
    DefinitionCount = { fg = starship.theme_blue },
    DefinitionIcon = { fg = starship.theme_blue },
    ReferencesIcon = { fg = starship.theme_blue },
    TargetWord = { fg = starship.theme_blue },

    -- Sneak
    Sneak = { fg = starship.theme5, bg = starship.theme_light },
    SneakScope = { bg = starship.theme2 },

    -- Cmp
    CmpItemKind = { fg = starship.theme_purple },
    CmpItemAbbrMatch = { fg = starship.theme_light, style = "bold" },
    CmpItemAbbrMatchFuzzy = { fg = starship.theme_light, style = "bold" },
    CmpItemAbbr = { fg = starship.theme_light },
    CmpItemMenu = { fg = starship.theme_green },

    -- Flash
    FlashBackdrop = { fg = starship.theme_dark },
    FlashLabel = { bg = starship.theme_red, bold = true, fg = starship.theme_light },

    -- Indent Blankline
    IndentBlanklineContextChar = { fg = starship.theme8 },
    IndentBlanklineChar = { fg = starship.theme7 },
    IblIndent = { fg = starship.theme7, nocombine = true },
    IblScope = { fg = starship.theme8, nocombine = true },

    -- Mini IndentScope
    MiniIndentscopeSymbol = { fg = starship.theme_dark, nocombine = true },
    MiniIndentscopePrefix = { nocombine = true }, -- Make it invisible

    -- Illuminate
    illuminatedWord = { bg = starship.theme1 },
    illuminatedCurWord = { bg = starship.theme1 },

    -- Lazy
    LazyUpdatesIndicator = { bg = starship.theme3, fg = starship.theme_yellow },

    -- Navic
    NavicIconsFile = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsModule = { fg = starship.theme_yellow, bg = starship.theme3 },
    NavicIconsNamespace = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsPackage = { fg = starship.theme_yellow, bg = starship.theme3 },
    NavicIconsClass = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsMethod = { fg = starship.theme_yellow, bg = starship.theme3 },
    NavicIconsProperty = { fg = starship.theme_green, bg = starship.theme3 },
    NavicIconsField = { fg = starship.theme_green, bg = starship.theme3 },
    NavicIconsConstructor = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsEnum = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsInterface = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsFunction = { fg = starship.theme_yellow, bg = starship.theme3 },
    NavicIconsVariable = { fg = starship.theme_purple, bg = starship.theme3 },
    NavicIconsConstant = { fg = starship.theme_purple, bg = starship.theme3 },
    NavicIconsString = { fg = starship.theme_green, bg = starship.theme3 },
    NavicIconsNumber = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsBoolean = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsArray = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsObject = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsKey = { fg = starship.theme_purple, bg = starship.theme3 },
    NavicIconsKeyword = { fg = starship.theme_purple, bg = starship.theme3 },
    NavicIconsNull = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsEnumMember = { fg = starship.theme_green, bg = starship.theme3 },
    NavicIconsStruct = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsEvent = { fg = starship.theme_orange, bg = starship.theme3 },
    NavicIconsOperator = { fg = starship.theme_light, bg = starship.theme3 },
    NavicIconsTypeParameter = { fg = starship.theme_green, bg = starship.theme3 },
    NavicText = { fg = starship.theme_light, bg = starship.theme3 },
    NavicSeparator = { fg = starship.theme_green, bg = starship.theme3 },

    -- nvim-dap
    DapBreakpoint = { fg = starship.theme_red },
    DapStopped = { fg = starship.theme_orange },

    -- nvim-dap-ui
    -- https://github.com/rcarriga/nvim-dap-ui/blob/master/lua/dapui/config/highlights.lua
    DapUIScope = { fg = starship.theme_yellow, style = "bold" },
    DapUIType = { fg = starship.theme_purple },
    DapUIModifiedValue = { fg = starship.theme_yellow, style = "bold" },
    DapUIDecoration = { fg = starship.theme_yellow },
    DapUIThread = { fg = starship.theme_green },
    DapUIStoppedThread = { fg = starship.theme_yellow, style = "bold" },
    DapUISource = { fg = starship.theme_purple },
    DapUILineNumber = { fg = starship.theme_yellow },
    DapUIFloatBorder = { fg = starship.theme_yellow },
    DapUIWatchesEmpty = { fg = starship.theme_yellow, style = "bold" },
    DapUIWatchesValue = { fg = starship.theme_green, style = "bold" },
    DapUIWatchesError = { fg = starship.theme_red, style = "bold" },
    DapUIBreakpointsPath = { fg = starship.theme_yellow, style = "bold" },
    DapUIBreakpointsInfo = { fg = starship.theme_green },
    DapUIBreakpointsCurrentLine = { fg = starship.theme_green, style = "bold" },
    DapUIBreakpointsDisabledLine = { fg = starship.theme8 },

    -- Hop
    HopNextKey = { fg = starship.theme_yellow, style = "bold" },
    HopNextKey1 = { fg = starship.theme_blue, style = "bold" },
    HopNextKey2 = { fg = starship.theme_light },
    HopUnmatched = { fg = starship.theme7 },

    -- Fern
    FernBranchText = { fg = starship.theme8 },

    -- nvim-ts-rainbow
    rainbowcol1 = { fg = starship.theme_yellow },
    rainbowcol2 = { fg = starship.theme_green },
    rainbowcol3 = { fg = starship.theme_purple },
    rainbowcol4 = { fg = starship.theme_blue },
    rainbowcol5 = { fg = starship.theme_blue },
    rainbowcol6 = { fg = starship.theme_red },
    rainbowcol7 = { fg = starship.theme_blue },

    -- lightspeed
    LightspeedLabel = { fg = starship.theme_blue, style = "bold" },
    LightspeedLabelOverlapped = { fg = starship.theme_blue, style = "bold,underline" },
    LightspeedLabelDistant = { fg = starship.theme_purple, style = "bold" },
    LightspeedLabelDistantOverlapped = { fg = starship.theme_purple, style = "bold,underline" },
    LightspeedShortcut = { fg = starship.theme_blue, style = "bold" },
    LightspeedShortcutOverlapped = { fg = starship.theme_blue, style = "bold,underline" },
    LightspeedMaskedChar = { fg = starship.theme_light, bg = starship.theme_dark, style = "bold" },
    LightspeedGreyWash = { fg = starship.theme8 },
    LightspeedUnlabeledMatch = { fg = starship.theme_light, bg = starship.theme2 },
    LightspeedOneCharMatch = { fg = starship.theme_blue, style = "bold,reverse" },
    LightspeedUniqueChar = { style = "bold,underline" },

    -- -- Notify
    -- NotifyBackground = { fg = starship.theme_purple, bg = starship.theme_purple },
    -- --- Border
    -- NotifyERRORBorder = { fg = util.darken(c.error, 0.3), bg = options.transparent and c.none or c.bg },
    -- NotifyWARNBorder = { fg = util.darken(c.warning, 0.3), bg = options.transparent and c.none or c.bg },
    -- NotifyINFOBorder = { fg = util.darken(c.info, 0.3), bg = options.transparent and c.none or c.bg },
    -- NotifyDEBUGBorder = { fg = util.darken(c.comment, 0.3), bg = options.transparent and c.none or c.bg },
    -- NotifyTRACEBorder = { fg = util.darken(c.purple, 0.3), bg = options.transparent and c.none or c.bg },
    -- --- Icons
    -- NotifyERRORIcon = { fg = c.error },
    -- NotifyWARNIcon = { fg = c.warning },
    -- NotifyINFOIcon = { fg = c.info },
    -- NotifyDEBUGIcon = { fg = c.comment },
    -- NotifyTRACEIcon = { fg = c.purple },
    -- --- Title
    -- NotifyERRORTitle = { fg = c.error },
    -- NotifyWARNTitle = { fg = c.warning },
    -- NotifyINFOTitle = { fg = c.info },
    -- NotifyDEBUGTitle = { fg = c.comment },
    -- NotifyTRACETitle = { fg = c.purple },
    -- --- Body
    -- NotifyERRORBody = { fg = c.fg, bg = options.transparent and c.none or c.bg },
    -- NotifyWARNBody = { fg = c.fg, bg = options.transparent and c.none or c.bg },
    -- NotifyINFOBody = { fg = c.fg, bg = options.transparent and c.none or c.bg },
    -- NotifyDEBUGBody = { fg = c.fg, bg = options.transparent and c.none or c.bg },
    -- NotifyTRACEBody = { fg = c.fg, bg = options.transparent and c.none or c.bg },

    -- ToggleTerm
    ToggleTermBorder = { fg = starship.theme_green, bg = starship.theme5 },
    ToggleTermBackground = { fg = starship.theme_light, bg = starship.theme2 },

    -- BufferLine
    BufferLineCloseButtonSelected = { fg = starship.theme_red, bg = starship.theme5 },
    BufferLineTabClose = { fg = starship.theme_red, bg = starship.theme5 },
    BufferLineIndicatorSelected = { fg = starship.theme_blue },
    BufferLineFill = { fg = starship.theme_dark, bg = starship.theme2 },

    -- Symbols Outline
    FocusedSymbol = { fg = starship.theme_yellow, style = "bold" },
  }
  -- Options:

  -- Disable nvim-tree background
  if vim.g.starship_disable_background then
    plugins.NvimTreeNormal = { fg = starship.theme_light, bg = starship.none }
  else
    plugins.NvimTreeNormal = { fg = starship.theme_light, bg = starship.sidebar }
  end

  if vim.g.starship_enable_sidebar_background then
    plugins.NvimTreeNormal = { fg = starship.theme_light, bg = starship.sidebar }
  else
    plugins.NvimTreeNormal = { fg = starship.theme_light, bg = starship.none }
  end

  return plugins
end

return theme
