local starship = (vim.o.background == "dark")
    and {
      theme1 = "#090d0f",
      theme2 = "#0d1216",
      theme3 = "#0f1519",
      theme4 = "#10171b",
      theme5 = "#12191e",
      theme6 = "#121d23",
      theme7 = "#182128",
      theme8 = "#23323b",
      theme9 = "#435d70",
      theme_blue = "#81A1C1",
      theme_green = "#A3BE8C",
      theme_dark = "#4C566A",
      theme_light = "#abb2bf",
      theme_red = "#BF616A",
      theme_orange = "#D08770",
      theme_yellow = "#EBCB8B",
      theme_purple = "#B48EAD",
      theme_brown = "#363620",
      none = "NONE",
    }
    or {
      theme1 = "#090d0f",
      theme2 = "#0d1216",
      theme3 = "#0f1519",
      theme4 = "#10171b",
      theme5 = "#12191e",
      theme6 = "#121d23",
      theme7 = "#182128",
      theme8 = "#23323b",
      theme9 = "#435d70",
      theme_blue = "#81A1C1",
      theme_green = "#A3BE8C",
      theme_dark = "#4C566A",
      theme_light = "#abb2bf",
      theme_red = "#BF616A",
      theme_orange = "#D08770",
      theme_yellow = "#EBCB8B",
      theme_purple = "#B48EAD",
      theme_brown = "#363620",
      none = "NONE",
    }

starship.sidebar = starship.theme2
starship.float = starship.theme2
starship.cursorlinefg = starship.theme4
starship.syntax = {
  ss1 = "#abb2bf",   -- Variable, Variable Builtin, Text, Underline, Literal, Namespace, Field
  ss2 = "#8FBCBB",   --  constBuiltin, ConstMacro, FuncMacro, Method, StringRegex
  ss3 = "#88C0D0",   -- PunctDelimiter, PunctBracket, PunctSpecial, Function, FuncBuiltin, KeywordFunction, KeywordReturn, KeywordOperator
  ss4 = "#81A1C1",   -- Constructor, Boolean, Include, Operator, Type, TypeBuiltin, Tag, Conditional, Keyword, Repeat
  ss5 = "#5E81AC",   -- Parameter, ParameterReference, Emphasis, Title, Property
  ss6 = "#BF616A",   -- Error
  ss7 = "#D08770",   -- Annotation
  ss8 = "#EBCB8B",   -- Constant
  ss9 = "#A3BE8C",   -- URI, String, Character
  ss10 = "#B48EAD",  -- Float, Number, Attribute, Exception, Label, Symbol, TagDelimiter, TextReference, StringEscape
  ss11 = "#23323b",  -- Comment
  ssdim = "#2e3440", -- Comment
}

return starship
