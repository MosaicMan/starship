local starship = require("starship.colors")

local lualine_ss = {}
local bold = false

lualine_ss.normal = {
  a = { bg = starship.theme1, fg = starship.theme_blue },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme_blue },
}

lualine_ss.insert = {
  a = { bg = starship.theme1, fg = starship.theme_yellow },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme_yellow },
}

lualine_ss.command = {
  a = { bg = starship.theme1, fg = starship.theme_green },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme_green },
}

lualine_ss.visual = {
  a = { bg = starship.theme1, fg = starship.theme_orange },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme_theme_orange },
}

lualine_ss.replace = {
  a = { bg = starship.theme1, fg = starship.theme_purple },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme_purple },
}

lualine_ss.terminal = {
  a = { bg = starship.theme1, fg = starship.theme_blue },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme_blue },
}

lualine_ss.inactive = {
  a = { bg = starship.theme1, fg = starship.theme1 },
  b = { bg = starship.theme3, fg = starship.theme_dark },
  c = { bg = starship.theme3, fg = starship.theme_dark },
  x = { bg = starship.theme3, fg = starship.theme_dark },
  y = { bg = starship.theme3, fg = starship.theme_dark },
  z = { bg = starship.theme1, fg = starship.theme1 },
}

if bold then
  for _, mode in pairs(lualine_ss) do
    mode.a.gui = "bold"
  end
end

return lualine_ss
